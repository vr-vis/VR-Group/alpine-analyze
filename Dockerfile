FROM alpine

RUN apk upgrade && apk add --no-cache --update \
                clang-extra-tools \
                git \
                python3 \
                expect

CMD ["/bin/sh"]
